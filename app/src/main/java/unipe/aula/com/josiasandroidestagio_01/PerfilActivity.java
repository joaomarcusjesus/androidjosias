package unipe.aula.com.josiasandroidestagio_01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PerfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
    }

    public void backView(View view){
        Intent i = new Intent (PerfilActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
