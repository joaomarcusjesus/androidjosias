package unipe.aula.com.josiasandroidestagio_01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by João Marcus on 15/03/2017.
 */

public class Contatos {


    private static List<String> contatoNomeList(){
        List<String> lista = new ArrayList<>();
        lista.add("EU");
        lista.add("ODEIO");
        lista.add("JAVA");
        lista.add("JAVA É UMA");
        lista.add("BOSTA");
        lista.add("COISA DE ...");
        return lista;
    }


    private static List<String> contatoTelList(){
        List<String> lista = new ArrayList<>();
        lista.add("66666666");
        lista.add("66666666-1");
        lista.add("66666666-2");
        lista.add("66666666-3");
        lista.add("66666666-4");
        lista.add("66666666-5");
        return lista;
    }


    public static List<Contato> getListaAtual(){
        List<Contato> listContacts = new ArrayList<>();

        for(int i = 0; i < contatoNomeList().size() && i < contatoTelList().size(); i++){
            Contato c = new Contato();
            c.setNome(contatoNomeList().get(i));
            c.setTelefone(contatoTelList().get(i));
            listContacts.add(c);
        }
        return listContacts;
    }
}
